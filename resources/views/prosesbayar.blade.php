@extends('template')
@section('content')



<div class="container-fluid">

    <div class="row" style="background-color: #172741!important">
            <div class="topnav col-md-4" style="background-color: #172741!important">
              <img src="img/icon/truk.png" class="headericon"> <span style="color: #ffffff">Estimasi biaya pengiriman.</span><
              <span><a href="" style="color: #ffffff; text-decoration: underline;">Selengkapnya</a></span>
            </div>  

            <div class="col-md-8" style="padding-left: 650px">
                <img src="img/icon/fbbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/wa.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/twitbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/gplusbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/igbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/youtubebulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
            </div>
    </div>
</div>

    
<div class="col-md-12" style=" box-shadow: 0px 2px 10px 4px #888888;">
<div class="col-md-3">
    <a href=""><img src="img/logo.png" class="active" href="#home"/></a>
</div>
<div class="col=md-9">
      <ul class="steps steps-12">
        <li><a href="#" title=""><em>KERANJANG BELANJA</em></a></li>
        <li class="current"><a href="#" title=""><em>INFORMASI PENGIRIMAN</em></a></li>
        <li><a href="#" title=""><em>PROSES PEMBAYARAN</em></a></li>
      </ul>
</div>

</div>
<div class="kosong"></div>
<div class="kosong"></div>
<div class="kosong"></div>



<div class="container"> <span class="mintamiring">Hi </span>
                        <span class="mintamiring">Adi Kurniadi</span>
                        <span class="mintamiring"> !</span></div>
<div class="kosong"></div>
<div class="container col-md-6" style="margin-left: 15px">
    <div class="row col-md-12 mintaborder" style="padding-left: 0px; padding-right: 0px">
        <div class="container-fluid headerbayar" style="background-color: #273746!important;
    font-weight: bold!important;
    font-style: italic!important;
    color: #ffffff!important;">
            <span class=""><u>Keterangan Pengiriman</u></span>
        </div>
        <div >
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Nama : </span><span class="mintamiring textketerangan">Adi Kurniadi Artana</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Email : </span><span class="mintamiring textketerangan">adi.kodohenkan@gmail.com</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">No. Telp : </span><span class="mintamiring textketerangan">089609277640</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Alamat : </span><span class="mintamiring textketerangan">Jl. Raya Sukawati No. 8x</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Negara : </span><span class="mintamiring textketerangan">Indonesia</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Provinsi : </span><span class="mintamiring textketerangan">Bali</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Kota : </span><span class="mintamiring textketerangan">Denpasar</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Kecamatan : </span><span class="mintamiring textketerangan">Denpasar Selatan</span>
            </div>
            <div class="col-md-12">
                <span class="mintamiring textketerangan">Kode Pos : </span><span class="mintamiring textketerangan">80582</span>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row col-md-6 mintaborder">
        <div>
            <span class="mintamiring"><u>Rincian Pembayaran</u></span>
        </div>
        <div>
                <div class="col-md-12">
                    <span class="mintamiring">SUB TOTAL</span>
                </div>
                <div class="col-md-12">
                    <span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">40.000</span><span class="mintamiring" style="color: #000000!important;
    font-style: 18px;
    float: right;">Rp </span>
                </div>
                <div class="col-md-12">
                    <span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">122.000</span><span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">Rp </span>
                </div>
                <div class="col-md-12">
                    <span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">122.000</span><span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">Rp </span>
                </div>
                <div class="col-md-12">
                    <span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">122.000</span><span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">Rp </span>
                </div>
        </div>
        <div>
                <div class="col-md-12">
                    <span class="mintamiring">BIAYA PENGIRIMAN</span>
                </div>
                <div class="col-md-12">
                    <span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">40.000</span><span class="mintamiring textharga" style="color: #000000!important;
    font-style: 18px;
    float: right;">Rp </span>
                </div>
        </div>
        <div>
                <div class="col-md-5">
                    <div class="">
                    <span class="mintamiring">KODE PROMO </span>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="" style="padding-bottom: 3px">
                        <input type="text" name="kdoepromo" style="margin-left: 0px" class="mintaborder">
                        <a style="background-color: #172741!important" class="btn btn-default" href="#" aria-label="check"><i style="color: white" class="fa fa-check" aria-hidden="true"></i></a>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" style="margin-top:10px!important; margin-right: 100px; padding-right: 400px">
        <button id="finish" type="submit" class="btn btn-default" style="float: right; background-color: #172741; color: white; font-style: italic; font-weight: bold;">Finish</button>

        <button type="submit" class="btn btn-default" style="float: right; background-color: #172741; color: white; font-style: italic; font-weight: bold;">Sebelumnya</button>
    </div>
</div>




<div class="kosong"></div>
<hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important; margin-bottom: 0px!important">

<div class="container-fluid" style="background-color: #273746">
    <div class="row text-center">
        <h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: white!important;"><u>Kenapa Memilih Kami</u></h3>
        <div class="col-md-4" style="padding-bottom: 30px;padding-top: 30px">
            <img src="img/icon/kualitas.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Bahan Kualitas terbaik</label>
        </div>
        <div class="col-md-4" style="padding-bottom: 30px; padding-top: 30px">
            <img src="img/icon/hasil.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Hasil Yang Memuaskan</label>
        </div>
        <div class="col-md-4" style="padding-bottom: 30px; padding-top: 30px">
            <img src="img/icon/cepat.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Proses Cepat</label>
        </div>
    </div>
</div>

<div class="kosong"></div>


<!-- MODAL POP UP -->




<div id="finishmodal" class="container modal">

  <!-- Modal content -->
  <div class="row modal-content">
    <div class="modal-header col-md-12">
        <div class="col-md-12" style="text-align: center;">
            <i class="fa fa-check fa-4x" aria-hidden="true" style="color: #172741;"></i>    
        </div>
        <div class="col-md-12" style="text-align: center;">
            <span class="mintamiring" style="font-size: 20px!important;"><u>Proses Pembelanjaan Anda</u></span>
        </div>
        <div class="col-md-12" style="text-align: center;">
            <span class="mintamiring" style="font-size: 20px!important;"><u>Telah Berhasil Di Proses</u></span>
        </div>
    </div>
    <div class="modal-body">
        <div class="col-md-12" style="margin-left: 45px">
              <div class="col-md-2">
                  <img src="{{ URL::asset('img/icon/fbblue.png') }}" class="sosmedpop">
              </div>
              <div class="col-md-2">
                  <img src="{{ URL::asset('img/icon/twit.jpg') }}" class="sosmedpop">
              </div>
              <div class="col-md-2">
                  <img src="{{ URL::asset('img/icon/ig.png') }}" class="sosmedpop">
              </div>
              <div class="col-md-2">
                  <img src="{{ URL::asset('img/icon/gplus.png') }}" class="sosmedpop">
              </div>
              <div class="col-md-2">
                  <img src="{{ URL::asset('img/icon/wa.png') }}" class="sosmedpop">
              </div>
          </div>
    </div>
    <div class="modal-footer">
      <div class="col-md-12" style="padding-bottom: 10px">
        <span style="margin-left: 20px">Dapatkan Informasi produk terbaru kami dan penawaran promo menarik lainnya</span>
      </div>
      <div class="col-md-8">
        <input type="text" name="subscribe" placeholder="Enter Your Email Address" style="height: 35px;width: 315px">
      </div>
      <div class="col-md-4">
        <a href="" class="btn btn-default" style="background-color: #0D1727;color: white;font-style: normal;font-weight: bold">SUPSCRIPE</a>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
    var modal = document.getElementById('finishmodal');

// Get the button that opens the modal
var btn = document.getElementById("finish");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>