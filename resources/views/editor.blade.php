@extends('template')
@section('content')


<div class="container-fluid">

    <div class="row" style="background-color: #172741!important">
            <div class="topnav col-md-4" style="background-color: #172741!important">
              <img src="img/icon/truk.png" class="headericon"> <span style="color: #ffffff">Estimasi biaya pengiriman. <span><a href=""><span style="color: #ffffff; text-decoration: underline;">Selengkapnya</span></a>
            </div>
            
            <div class="col-md-8" style="padding-left: 650px">
                <img src="img/icon/fbbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/wa.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/twitbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/gplusbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/igbulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
                <img src="img/icon/youtubebulet.png" style="max-height: 30px;max-width: 30px;margin-top: 3px">
            </div>
    </div>
</div>
<div class="row">
            <nav class="navbar navbar-default" role="navigation">
                      <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                            <li><a href=""><img src="img/logo.png" class="active" href="#home"/></a></li>
                            <li class="dropdown mega-dropdown menubar">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Product<span class="caret"></span></a>       
                                <div class="dropdown-menu mega-dropdown-menu" style="padding-bottom: 0px!important">
                                            <div class="container-fluid">
                                        <!-- Tab panes -->
                                                <div class="tab-content">
                                                      <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="#"><img src="img/menu/x_banner.png" class="picmenu"><span>X Banner</span></a></li>
                                                            <li><a href="#"><img src="img/menu/mug.jpg" class="picmenu"><span>Mug</span></a></li>
                                                            <li><a href="#"><img src="img/menu/book.jpg" class="picmenu"><span>Album</span></a></li>
                                                            <li><a href="#"><img src="img/menu/acrylic.jpg" class="picmenu"><span>Photo Acrylic</span></a></li>
                                                            <li><a href="#"><img src="img/menu/canvas.jpg" class="picmenu"><span>Canvas</span></a></li>
                                                            <li><a href="#"><img src="img/menu/flyers.png" class="picmenu"><span>Flyers</span></a></li>
                                                            <li><a href="#"><img src="img/menu/kalender.jpg" class="picmenu"><span>Calendar</span></a></li>
                                                        </ul>
                                                        <div class="row" style="background-color: #172741;padding-bottom: 0px!important;margin-bottom: 0px!important">
                                                      <span style="color: #ffffff">All Product</span>
                                                  </div>      
                                                      </div>
                                                </div>
                                            </div>
                                                                
                                </div>                                                          
                          </li>
                            <li class="menubar"><a href="#" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">How to Order</a></li>
                            <li class="menubar"><a href="#" style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">About Us</a></li>
                            </ul>
                            <ul class="nav-list list-inline">
                                <li style="float: right!important; text-align: right!important; width: auto;"> <a class="btn btn-success" href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a></li>
                                <li style="float: right!important; text-align: right!important; width: auto; ">
                                <h4>0 ITEM</h4>
                            </li>
                            <li style="float: right!important; text-align: right!important; width: auto;">
                                <a href="#"><img src="img/icon/keranjang.png" style="max-width: 30px; max-height: 30px;"></a>
                            </li>
                            </ul>
                            
                        </div><!-- /.navbar-collapse -->
                      </div><!-- /.container-fluid -->
                    </nav>              
</div>

<!-- BOX EDITOR -->
<div class="container">
  <div class="row">
      <div class="col-md-9 bordereditor">
        <div class="editormug">
            <img src="img/mug/contoh.jpg" style="max-width: 700px">
        </div>
      </div>

      <div class="col-md-3">
        <button class="btn btn-default btneditor" type="button"><span class="fa fa-4x fa-arrow-circle-up"><span style="font-size: 10px; vertical-align: middle;font-style: italic;font-weight: bold;"> UPLOAD GAMBAR</span></span></button>
      </div>

      <div class="col-md-3">
        <button class="btn btn-default btneditor" type="button"><span class="fa fa-4x fa-font"><span style="font-size: 10px; vertical-align: middle;font-style: italic;font-weight: bold;"> SEMATKAN TEKS</span></span></button>
      </div>

      <div class="col-md-3">
        <button class="btn btn-default btneditor" type="button"><span class="fa fa-4x fa-paperclip"><span style="font-size: 8.5px; vertical-align: middle;font-style: italic;font-weight: bold;"> SEMATKAN CLIPART</span></span></button>
      </div>

      <div class="col-md-3">

      </div>
  </div>
</div>
  
<div>
    <hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important;">
</div>

<!-- SLIDER MUG -->
<div class="container">
    <div class="row text-center">
        <h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Produk Lainnya</h3>
    </div>
    <div class="kosong"></div>

  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media3">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>                 
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>           
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>                  
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>      
               <!-- batas -->
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>               
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>      
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>      
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>          
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>             
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>      
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>      
              <div class="item  col-xs-3 col-lg-3">
                <div class="thumbnail"><img class="group list-group-image kotak-dalam" src="img/mug/mugedit1.jpg" alt="" />
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h4 class="group inner list-group-item-heading">Merchandise</h4>
                                <p class="pricing">
                                    Start From 25.000</p>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <a class="btn btn-sm btn-success" href="http://www.jquery2dotnet.com" style="font-size:11px">Design Now</a>
                            </div>
                        </div>
                    </div>
                </div>
               </div>         
            </div>
          </div>
        </div>
        <a data-slide="prev" href="#media3" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media3" class="right carousel-control">›</a>
      </div>                          
    </div>
  </div>
</div>

</div>

<div>
    <hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important;margin-top: 0px;padding-top: 0px;margin-bottom: 0px">
</div>
<div class="container-fluid" style="background-color: #F2F2F2">
  <div class="row text-center">
      <h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: #172741!important;">Hasil Produk
                </h3>
              <div style="margin-left: 140px">
                <div class="col-md-2">
                  <img class="hasilproduk" src="img/mug/item.png">  
                </div>
                <div class="col-md-2">
                  <img class="hasilproduk" src="img/mug/item.png">  
                </div>
                <div class="col-md-2">
                  <img class="hasilproduk" src="img/mug/item.png">  
                </div>
                <div class="col-md-2">
                  <img class="hasilproduk" src="img/mug/item.png">  
                </div>
                <div class="col-md-2">
                  <img class="hasilproduk" src="img/mug/item.png">  
                </div>
              </div>
                <div class="kosong"></div>
  </div>
</div>

<div class="container-fluid" style="background-color: #273746;padding-bottom: 0">
    <div class="row text-center">
        <h3 style="font-size: 20px!important; font-weight: bold; font-style: italic; color: white!important;"><u>Bagaimana Cara Mendapatkannya</u></h3>
        <div class="col-md-4" style="padding-bottom: 30px;padding-top: 30px">
            <img src="img/icon/jari.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Pilih Produk Terbaik Kami</label>
        </div>
        <div class="col-md-4" style="padding-bottom: 30px; padding-top: 30px">
            <img src="img/icon/cat.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Desain Sendiri Dengan Mudah</label>
        </div>
        <div class="col-md-4" style="padding-bottom: 30px; padding-top: 30px">
            <img src="img/icon/trukoren.png" style="max-height: 100px; max-width: 200"> <label class="control-label" style="color: white; font-weight: bold; font-style: italic;"> Tinggal Tunggu Di Rumah</label>
        </div>
    </div>
</div>


<div>
    <hr style="background-color: #999999!important; border: solid 3px #999999!important; height: 5px!important;margin-top: 0px;padding-top: 0x">
</div>


